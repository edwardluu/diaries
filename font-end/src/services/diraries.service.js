import { API_URL } from '../environment';
const axios = require('axios')



export const diariesService = {
    getDiaries,
    getDiary,
    createDiary,
    updateDiary,
    deleteDiary
}

function getDiaries () {
    let item = localStorage.getItem('data');
    let response = JSON.parse(item);
    const headers = response.headers;
    const user = response.data.data;
    const url = API_URL + 'diaries';
    return   axios.get( url , { headers})
        .then( 
            response => {
                const daries_user = response.data.filter( x =>  x.user_id === user.id);
                return daries_user;
        })
        .catch( (error)  =>{
            
        });
}
function getDiary(id) {
    let item = localStorage.getItem('data');
    let response = JSON.parse(item);
    const headers = response.headers;
    const url = API_URL + 'diaries/' + id ;
    return axios.get(url, { headers})
    .then( 
        response => {
            return response.data;
    })
    .catch( (error)  =>{
        return error.response;
    });
}
function createDiary(diary) {
    let item = localStorage.getItem('data');
    let response = JSON.parse(item);
    const url = API_URL + 'diaries';
    const headers = response.headers;
    return axios.post(url,  diary , {headers})
    .then( response => {
        return response.data
    })
    .catch(
        errors => {
            return errors.response
        }
    )
}
function updateDiary(diary , id) {
    let item = localStorage.getItem('data');
    let response = JSON.parse(item);
    const url = API_URL + 'diaries/' + id;
    const headers = response.headers;
    return axios.put(url,  diary , {headers})
    .then( response => {
        return response.data
    })
    .catch(
        errors => {
            return errors.response
        }
    )
}
function deleteDiary(id) {
    let item = localStorage.getItem('data');
    let response = JSON.parse(item);
    const url = API_URL + 'diaries/' + id;
    const headers = response.headers;
    return axios.delete(url,  {headers})
    .then( response => {
        return response.data
    })
    .catch(
        errors => {
            return errors.response
        }
    )
}