import React, { Component } from "react";
import Moment from 'react-moment';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { diariesService } from '../services/diraries.service';


const styles = theme => ({
    fab: {
        margin: theme.spacing.unit,
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
    appBar: {
        position: 'relative',
      },
      flex: {
        flex: 1,
    },
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class DiaryDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            fields: {
                title: '',
                content: ''
            },
            open: false,
            open_dialog: false
        }
    }
    componentDidMount() {
        diariesService.getDiary(this.props.match.params.id)
            .then(
                result => {
                    this.setState({
                        data: result
                    })
                }
            )
    }
    handleClickOpen = () => {
        this.setState({
            fields: {
                title: this.state.data.title,
                content: this.state.data.content
            },
        })
        this.setState({ open: true });
      };
    
      handleClose = () => {
        this.setState({ open: false });
      };
      handleChange = name => event => {
        this.setState({
          fields: {
            ...this.state.fields,
            [name]: event.target.value
          }
        });
      };
      handleSubmitUpdateDiary = event => {
        event.preventDefault();
        diariesService.updateDiary(this.state.fields , this.props.match.params.id)
        .then(
            response => {
                this.setState({ data: response});
                this.setState({ open: false });
            }
        )
      }

      handleClickDelete = () => {
        this.setState({ open_dialog: true });
      }

      handleCloseDialog = () => {
        this.setState({ open_dialog: false });
      }
      handleAgreeDialog = () => {
        diariesService.deleteDiary(this.props.match.params.id)
        .then(
                response => {
                    this.setState({ open: false });   
                    this.props.history.push('/');
                }
        )
      }
    render() {
        const { data } = this.state;
        const { classes } = this.props;
        const { title , content } = this.state.fields;
        return (
            <div className="Diary">
                <h1 className="title-form">{data.title}</h1>
                <div className="wrap-action-diary">
                    <div>
                        <p className="date-diary">
                            <i className="material-icons">event_note</i>
                            <Moment format="D MMM YYYY" withTitle>
                                {data.created_at}
                            </Moment>
                        </p>
                    </div>
                    <div>
                        <Tooltip title="Edit Diary" aria-label="Edit">
                            <Fab onClick={this.handleClickOpen} color="primary" aria-label="Edit" className={classes.fab}>
                                <Icon>edit_icon</Icon>
                            </Fab>
                        </Tooltip>
                        <Tooltip onClick= {() => this.handleClickDelete()} title="Delete Diary" aria-label="Delete">
                            <Fab aria-label="Delete" className={classes.fab}>
                                <DeleteIcon />
                            </Fab>
                        </Tooltip>
                    </div>
                </div>
                <p className="content-diary">{data.content}</p>
                <Dialog fullScreen
                        open={this.state.open}
                        onClose={this.handleClose}
                        TransitionComponent={Transition}>
                    <form onSubmit={this.handleSubmitUpdateDiary} validate="true" autoComplete="off">
                        <AppBar className={classes.appBar}>
                            <Toolbar>
                                <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                                    <CloseIcon />
                                </IconButton>
                                <Typography variant="h6" color="inherit" className={classes.flex}>
                                    My Daily Life
                                </Typography>
                                <Button color="inherit" type="submit"> Save </Button>
                            </Toolbar>
                        </AppBar>
                        <div>
                            <TextField id="standard-title-input"
                                label="Title"
                                className={classes.textField}
                                placeholder="Title"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                onChange={this.handleChange('title')}
                                value={title}
                                required
                                type="text"
                                InputLabelProps={{
                                    shrink: true,
                                }} />

                            <TextField
                                id="outlined-multiline-flexible"
                                label="Content"
                                multiline
                                fullWidth
                                required
                                rows="20"
                                rowsMax="50"
                                value={content}
                                placeholder="Hi ,what are you doing today?"
                                onChange={this.handleChange('content')}
                                className={classes.textField}
                                margin="normal"
                                variant="outlined"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </div>
                    </form>
                </Dialog>
                <Dialog
                    open={this.state.open_dialog}
                    onClose={this.handleCloseDialog}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle id="alert-dialog-title">{"Notification"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Are you sure that you want remove your diary?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseDialog} color="primary">
                        Disagree
                        </Button>
                        <Button onClick={this.handleAgreeDialog} color="primary" autoFocus>
                        Agree
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }

}
DiaryDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(DiaryDetails);
