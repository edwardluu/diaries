import React, { Component } from "react";
import Loading from '../component/loading';
import { diariesService } from '../services/diraries.service';
import MaterialTable from 'material-table'
import Moment from 'react-moment';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
})
function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
      open: false,
      fields: {
        title: '',
        content: ''
      }
    }
  }
  componentDidMount() {

    diariesService.getDiaries()
      .then(result => {
        if(result) {
          this.setState(
            {
              data: result,
              loading: false
            }
          )
        }
      })
  }
  handleClickOpen = () => {
    this.setState({ 
      open: true,
      fields: {
        title: '',
        content: ''
      }  
     });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = name => event => {
    this.setState({
      fields: {
        ...this.state.fields,
        [name]: event.target.value
      }
    });
  };
  handleSubmitNewDiary = event => {
    const {data} = this.state;
    event.preventDefault();
    diariesService.createDiary(this.state.fields)
    .then(
      response => {
          data.push(response);
          this.setState({ open: false });
      }
    )
  }
  render() {
    const { loading, data  } = this.state;
    const { title , content } = this.state.fields;
    const { classes } = this.props;
    if (loading) {
      return (<Loading />)
    }
    return (
      <div className="Home" >
        <div className="wrap-add">
          <h2 className="title-form">My Diaries</h2>
          <Button onClick={this.handleClickOpen} variant="outlined" color="primary" className="add-diary">
            <i className="material-icons add-icon">library_books</i>
            New Diary
            </Button>
        </div>
        <MaterialTable
          columns={[
            { title: 'Title', field: 'title' },
            {
              title: 'Wrote Date',
              render: rowData => {
                const date = rowData.created_at
                return (
                  <div style={{ color: '#ff9800' }}>
                    <Moment format="DD/MM/YYYY">{date}</Moment>
                  </div>

                )
              }
            }
          ]}
          data={data}
          title="List Diaries"
          options={{
            columnsButton: true,
            pageSize: 5,
          }}
          actions={[
            {
              icon: 'looks',
              tooltip: 'Show Diary Info',
              onClick: (event, rowData) => {
                const id = rowData.id;
                this.props.history.push('/' + id)
              },
              iconProps: {
                style: {
                  fontSize: 30,
                  color: 'peachpuff',
                },
              },
            },
          ]}
          localization={{
            actions: 'Details',
          }}
        />
        <Dialog
          fullScreen
          open={this.state.open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
        >
        <form onSubmit={this.handleSubmitNewDiary} validate="true">
        <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.flex}>
                  My Daily Life
              </Typography>
              <Button color="inherit" type="submit">
                save
              </Button>
            </Toolbar>
          </AppBar>
          <div>
            <TextField  id="standard-title-input"
                        label="Title"
                        className={classes.textField}
                        placeholder="Title"
                        fullWidth
                        margin="normal"
                        variant="outlined"
                        onChange={this.handleChange('title')}
                        value={title}
                        required
                        type="text"
                        InputLabelProps={{
                          shrink: true,}} />

         <TextField
          id="outlined-multiline-flexible"
          label="Content"
          multiline
          fullWidth
          required
          rows="20"
          rowsMax="50"
          value={content}
          placeholder="Hi ,what are you doing today?"
          onChange={this.handleChange('content')}
          className={classes.textField}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,}}
        />
          </div>
        </form>


        </Dialog>
      </div>
    )
  };
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Home);