import React, { Component } from "react";
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../action/user.action';


const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  dense: {
    marginTop: 19,
  },
});

class Login extends Component {  
  constructor(props){
    super(props);
    this.props.dispatch(userActions.logout());
    this.state={
      fields: {
        email: '',
        password:'',
      },
      submitted: false,
      error: {
        valid_length: false,
        valid_email: false
      }
    }
  }
  componentDidMount() {
    
  }
  handleSubmit = event => {
    event.preventDefault();
  
    this.setState({
      submitted: true
    });
    const { dispatch } = this.props;
    if(this.checkEmail() && this.checkPassword()) {
      dispatch(userActions.login(this.state.fields));
    }
  }
  checkPassword = ()  => {
    if(this.state.fields.password.length < 8) {
          this.setState({
            error: {valid_length: true }
          })
          return false;
    } else {
      this.setState({
        error: {valid_length: false }
      })
      return true;
    }
}
  
  checkEmail = () => {
    const regex = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/
    if (!regex.test(this.state.fields.email)) {
        this.setState({
          error: {valid_email: true }
        })
        return false;
    } else {
      this.setState({
        error: {valid_email: false }
      })
      return true;
    }
  }
  handleChange = name => event => {
    this.setState({
      fields: {
        ...this.state.fields,
        [name]: event.target.value
      }
    });
  };
    render() {
      const { classes } = this.props;
      const { email , password } = this.state.fields;
      const { alert } = this.props;
      return(
        <div className="Form">
            <div className="Login">
          <h1 className="title-form">Sign In</h1>
          {alert.message ?
                            <div className={`alert ${alert.type}`}>{alert.message}</div> : <div> &nbsp;</div>
                        }
          <form className="form-login"  onSubmit={this.handleSubmit} validate="true" autoComplete="on">
          <TextField  id="standard-email-input"
                        label="Email"
                        className={classes.textField}
                        type="email"
                        required
                        value={email}
                        onChange={this.handleChange('email')}
                        onBlur={() => this.checkEmail()}
                        error={this.state.error.valid_email} 
                        helperText={this.state.error.valid_email ? 'Please enter a valid email address' : ' '}
                        margin="normal" />
                    
            <TextField  id="standard-password-input"
                        label="Password"
                        className={classes.textField}
                        type="password"
                        inputProps={{
                          maxLength: 16,
                        }}
                        required
                        value={password}
                        onChange={this.handleChange('password')}
                        margin="normal"
                        onBlur={() => this.checkPassword()}
                        error={this.state.error.valid_length}  
                        helperText={this.state.error.valid_length ? 'Your password must be between 8-16 characters' : ' '} />
                      
                <Button fullWidth={true}  className="button-form" type="submit" variant="contained" color="primary" >Log in</Button>      
          </form>
            
        </div>
            <div className="RedirectLinkForm">
              <span className="text-link"> Don't have an account?</span>
              <Link to="/register" className="btn btn-link link-form">Sign up</Link>
            </div>
        </div>
       
      )  
    };
} 

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  }
}
Login.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(
  connect(mapStateToProps)(Login)
)