import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles'
import { userActions } from "../action/user.action";
import { Link } from 'react-router-dom';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  dense: {
    marginTop: 19,
  },
});
class Register extends Component {  
  constructor(props){
    super(props);
    this.state={
      fields: {
        email: '',
        password:'',
        password_confirmation: ''
      },
      error: {
        valid_repass: false,
        valid_length: false,
        valid_email: false
      }
    }
  }
  
  handleSubmit = event => {
     event.preventDefault();
     const { dispatch } = this.props;
     const { fields } = this.state;
      if( this.checkPassword() && this.checkRePassword() && this.checkEmail()) {
          dispatch(userActions.register(fields))
      }
    }

  handleChange = name => event => {
    this.setState({
      fields: {
        ...this.state.fields,
        [name]: event.target.value
      } 
    });
  };

  checkPassword = ()  => {
      if(this.state.fields.password.length < 8) {
            this.setState({
              error: {valid_length: true }
            })
            return false;
      } else {
        this.setState({
          error: {valid_length: false }
        })
        return true;
      }
  }
  checkRePassword = () => {
    if (this.state.fields.password_confirmation !==  this.state.fields.password) {
        this.setState({
          error: {valid_pass: true }
        })
        return false;
    } else {
      this.setState({
        error: {valid_pass: false }
      })
      return true;
    }
  }
  checkEmail = () => {
    const regex = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/
    if (!regex.test(this.state.fields.email)) {
        this.setState({
          error: {valid_email: true }
        })
        return false;
    } else {
      this.setState({
        error: {valid_email: false }
      })
      return true;
    }
  }
    render() {
      const { classes } = this.props;
      const { email , password , password_confirmation } = this.state;
      const { alert } = this.props;
      return(
        <div className="Form">
           <div className="Register Login">
          <h1 className="title-form">Sign Up</h1>
          {
            alert.message ?  <div className={`alert ${alert.type}`}>{alert.message}</div> : <div> </div>
          }
          <form onSubmit={this.handleSubmit} className="form-register" validate="true" autoComplete="on">
          <TextField  id="standard-email-input"
                        label="Email"
                        className={classes.textField}
                        type="email"
                        required
                        value={email}
                        onChange={this.handleChange('email')}
                        onBlur={() => this.checkEmail()}
                        error={this.state.error.valid_email} 
                        helperText={!this.state.error.valid_email ? ' ' : 'Please enter a valid email address' }
                        margin="normal" />
            <TextField  id="standard-password-input"
                        label="Password"
                        className={classes.textField}
                        type="password"
                        inputProps={{
                          maxLength: 16,
                        }}
                        required
                        value={password}
                        onChange={this.handleChange('password')}
                        margin="normal"
                        onBlur={() => this.checkPassword()}
                        error={this.state.error.valid_length}  
                        helperText={!this.state.error.valid_length ? ' ' : 'Your password must be between 8-16 characters' } />
            <TextField  id="standard-re-password-input"
                        label="Re-Enter Password"
                        className={classes.textField}
                        type="password"
                        inputProps={{
                          maxLength: 16,
                        }}
                        required
                        value={password_confirmation}
                        onBlur={() => this.checkRePassword()}
                        onChange={this.handleChange('password_confirmation')}
                        margin="normal" 
                        error={this.state.error.valid_pass}  
                        helperText={!this.state.error.valid_pass ? ' ' : 'Password do not match'}/>
                <Button type="submit" variant="contained" fullWidth={true} color="primary" className="button-form">submit</Button>
          </form>
        </div>
          <div className="RedirectLinkForm">
                <span className="text-link"> Don't have an account?</span>
                <Link to="/Login" className="btn btn-link link-form">Login</Link>
          </div>
        </div>
      )  
    };
} 

Register.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { alert } = state;
  return {
      alert
  };
}

export default withStyles(styles)(
  connect(mapStateToProps)(Register)
)