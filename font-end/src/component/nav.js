import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';

import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { history } from '../constants';
import SvgIcon from '@material-ui/core/SvgIcon';
import Grid from '@material-ui/core/Grid';
import { userActions } from '../action/user.action';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 3,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  icon: {
    margin: theme.spacing.unit * 2,
  },
  iconHover: {
    margin: theme.spacing.unit * 2,
    '&:hover': {
    },
  },
});
function HomeIcon(props) {
  return (
    <SvgIcon {...props}>
      <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
    </SvgIcon>
  );
}

class MenuAppBar extends React.Component {
  state = {
    anchorEl: null
  };
  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  redirectProfile = () => {
    this.setState({ anchorEl: null });
    history.push('profile');
  }
  logOut = () =>  {
     userActions.logout();
     history.push('login');
  }

  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
          <Grid  justify="space-between" container 
      spacing={24}>
          <Grid item className="icon-home">
              <a href="/"><HomeIcon fontSize="large" color="secondary"/></a>
           </Grid> 
           <Grid item>
           <div>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="secondary"
                  fontSize="large"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.redirectProfile}>Profile</MenuItem>
                  <MenuItem onClick={this.logOut}>Log Out</MenuItem>
                </Menu>
              </div>
           </Grid>       
          </Grid>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

MenuAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);