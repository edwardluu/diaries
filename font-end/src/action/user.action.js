import { userService }  from '../services/user.service';
import { userConstants } from '../constants';
import { alertActions } from './alert.actions';
import { history } from '../constants';
import { API_URL } from '../environment';
const axios = require('axios');
export const userActions = {
    login,
    register,
    logout
}
function login(user ) {
    return dispatch => {
        dispatch(request({user}));

        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
          };
          const url = API_URL + 'auth/sign_in';
        axios.post(url,  user , axiosConfig)
          .then(function (response) {
            const user = response.data;
            const token = response.headers['access-token']
           dispatch(success(user));
            if (token !== '') {
                localStorage.setItem('data',JSON.stringify(response))
                history.push('/');
            }
          })
          .catch( (error)  =>{
            const  errors  = 'Email or password is incorrect';
            dispatch(fail(errors.toString()));
            dispatch(alertActions.error(errors));
          });
    }
    function request(user) { return {type: userConstants.LOGIN_REQUEST , user }}
    function success(user) { return {type: userConstants.LOGIN_SUCCESS , user }}
    function fail(error) { return {type: userConstants.LOGIN_FAILURE , error}}
}

function register(user) {
    return dispatch => {
       dispatch(request(user));
       let axiosConfig = {
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            "Access-Control-Allow-Origin": "*",
        }
      };
      const url = API_URL + 'auth';
    axios.post( url ,  user , axiosConfig)
      .then(function (response) {
        const user = response.data.data
        dispatch(success(user));
        history.push('/login');
        dispatch(alertActions.success('Registration successful'));
      })
      .catch( (error)  =>{
       const  errors  = error.response.data.errors.full_messages[0];
        dispatch(fail(errors.toString()));
        dispatch(alertActions.error(errors));
      })
    };
    function request(user) { return {type: userConstants.REGISTER_REQUEST , user }}
    function success(user) { return {type: userConstants.REGISTER_SUCCESS , user }}
    function fail(error) { return {type: userConstants.REGISTER_FAILURE , error}}
 
}
function logout() {
    userService.logout();   
    return { type: userConstants.LOGOUT};
}