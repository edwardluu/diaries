import React, { Component } from 'react';
import { Router, Route, Switch} from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';
import { alertActions } from './action/alert.actions';
import { PrivateRoute } from './component/private_router';
import { history } from './constants/history';
import Home from './pages/home';
import MenuAppBar from './component/nav';
import Login from './pages/login';
import Register from './pages/register';
import Profile from './pages/profile';
import DiaryDetails from './pages/dirary'


class App extends Component {
  constructor(props) {
    super(props);

    const { dispatch } = props;
    history.listen((location, action) => {
      // clear alert on location 
        dispatch(alertActions.clear());
    });
  }
  componentDidMount() {
  
  }

  render() {
    return (
      <Router history={history}>
       <div className="App">
           {localStorage.length > 0 &&
            <MenuAppBar/>   
          }
            <Switch>
              <Route  path="/login" component={Login} />
              <Route path="/register" component={Register}/>
              <PrivateRoute exact path="/" component={Home} />
              <PrivateRoute path="/profile" component={Profile} />
              <PrivateRoute  path="/:id" component={DiaryDetails} />
            </Switch>
        </div>
    </Router>

    );
  }
}
function mapStateToProps(state) {
  const { alert } = state;
  return {
     alert
  }
}
 const connectApp = connect(mapStateToProps)(App);
export { connectApp as App };
