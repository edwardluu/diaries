class DiariesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_diary, only: [:show, :update, :destroy]
    def index
        @diaries = Diary.all
        render json: @diaries
    end
    #Post /diary
    def create
        @diary = Diary.new(diary_params.merge(user_id: current_user.id))
        if @diary.save
            render json: @diary , status: :created
        else
            render json: @diary.errors, status: :unprocessable_entity
        end
    end
    #Get /diaries/:id
    def show 
        render json: @diary
    end
    #Put /diaries/:id
    def update
       if @diary.update(diary_params) 
            render json: @diary
        else
            render json: @diary.errors, status: :unprocessable_entity
        end
    end
    #Delete /diaries/:id
    def destroy
        @diary.destroy
        head :no_content
    end

    private 
    def set_diary 
        @diary = Diary.find(params[:id])
    end
    def diary_params
        params.permit(:title , :content)
    end

    
end

